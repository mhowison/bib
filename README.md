# Bioinformatics Brew

BiB is a cross-platform manager for command-line bioinformatics tools. This 

For more information and examples of how to use BiB,
see [http://bib.bitbucket.org](http://bib.bitbucket.org).

## Installing

See [http://bib.bitbucket.org/install.html](http://bib.bitbucket.org/install.html).

## Contributing

Recipes are simple INI-style configuration files that are parsed with the Python
ConfigParser class. We will provide more documentation on the different keys used
by these files soon. In the meantime, please look at the existing recipes to see
how they work.

You can contribute your own recipes in two different ways:

* For single recipes, create an [issue](https://bitbucket.org/mhowison/bib/issues)
  and paste the recipe into the body.
* If you plan to contribute many recipes, please fork this repo on Bitbucket
  and create a separate pull request for each recipe in a branch with the recipe
  name.

We will review and test each recipe before committing it to the master branch,
which will make it available to users through the `bib update` command.

## Authors

BiB is created and maintained by Mark Howison and Aaron Shen at the
[Center for Computation and Visualization](https://www.ccv.brown.edu),
Brown University.

## License

Copyright 2013-2016, Brown University, Providence, RI. All Rights Reserved.

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
International License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
