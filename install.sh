#!/bin/bash

set -e

PREFIX=${1-"/opt/bib"}

USER=${USER-$(id -nu)}
GROUP=$(id -ng $USER)

OS=""
echo "Detecting operating system..."
UNAME=$(uname)
if [ "$UNAME" == "Linux" ]
then
  if [ -f /etc/redhat-release ]
  then
    if [ $(grep -c "CentOS Linux release 7" /etc/redhat-release) -gt 0 ]
    then
      echo "CentOS 7 detected."
      OS="centos7"
    elif [ $(grep -c "CentOS release 6" /etc/redhat-release) -gt 0 ]
    then
      echo "CentOS 6 detected."
      OS="centos6"
    else
      echo "Unsupported version of RedHat:"
      cat /etc/redhat-release
      exit 1
    fi
  elif [ -f /etc/lsb-release ]
  then
    if [ $(grep -c "^DISTRIB_RELEASE=16" /etc/lsb-release) -gt 0 ]
    then
      echo "Ubuntu 16 detected"
      OS="ubuntu16"
    elif [ $(grep -c "^DISTRIB_RELEASE=15" /etc/lsb-release) -gt 0 ]
    then
      echo "Ubuntu 15 detected"
      OS="ubuntu15"
    elif [ $(grep -c "^DISTRIB_RELEASE=14" /etc/lsb-release) -gt 0 ]
    then
      echo "Ubuntu 14 detected."
      OS="ubuntu14"
    else
      echo "Unsupported version of Ubuntu:"
      grep "^DISTRIB_RELEASE=" /etc/lsb-release
      exit 1
    fi
  else
    echo "Unsupported distribution of Linux."
    exit 1
  fi 
elif [ "$UNAME" == "Darwin" ]
then
  VERSION=$(/usr/bin/sw_vers -productVersion)
  if [ "${VERSION:0:5}" == "10.11" ]
  then
    echo "Mac OS X 10.11 detected."
    OS="osx10.11"
  else
    echo "Unsupported version of Mac OS X:"
    echo $VERSION
    exit 1
  fi
else
  echo "Unsupported operating system."
  exit 1
fi

echo "Installing to prefix: $PREFIX"

SUDO="sudo"
if [ $# -gt 0 ]; then
  export BIB_PREFIX=$PREFIX
  echo "Installing without sudo."
  SUDO=""
fi

$SUDO mkdir -p $PREFIX

echo "Changing ownership of $PREFIX to ${USER}:${GROUP}."

$SUDO chown $USER:$GROUP $PREFIX

echo "Checking for git..."

git --version

echo "Downloading Bioinformatics Brew repository..."

if [ -d $PREFIX/.git ]
then
  cd $PREFIX
  git pull
else
  git clone https://bitbucket.org/mhowison/bib $PREFIX
  cd $PREFIX
fi

echo "Creating directory tree."

mkdir -p $PREFIX/active/include
mkdir -p $PREFIX/active/lib
mkdir -p $PREFIX/active/lib/pkgconfig
for i in {1..7}
do
  mkdir -p $PREFIX/active/man/man$i
done
echo $OS >$PREFIX/ARCH

echo "Installing development kit..."

DEVKIT="bib-devkit-$OS-r2.tar.bz2"
DEVKIT_URL="https://bitbucket.org/mhowison/bib/downloads/$DEVKIT"

curl -kL $DEVKIT_URL >$DEVKIT
tar xjf $DEVKIT
rm $DEVKIT

echo "Preparing database for first use."
lib/bib-admin rebuild

echo "Done!"

BIN="$PREFIX/active/bin"
echo "
  Bioinformatics Brew has been installed successfully.

  To use the 'bib' command, as well as all installed software, you must add
  '$BIN'
  to the PATH variable in your environment, e.g. with:

  echo \"export PATH=$BIN:\\\$PATH\" >>~/.bash_profile
"

if [ $# -gt 0 ]; then
echo "  Because you installed to a custom prefix (instead of /opt/bib), you
  also need to set the BIB_PREFIX variable:

  echo \"export BIB_PREFIX=$PREFIX\" >>~/.bash_profile

"
fi

echo "  Then close and reopen your shell."
