# Copyright 2013-2014, Brown University, Providence, RI.
# All Rights Reserved.
#
# This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
# International License. To view a copy of this license, visit
# http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
# Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

import os
import subprocess
import sys

from config import BIB_ARCH

def die(*messages):
  """
  Prints "bib:" and a list of `messages`, then aborts.
  """
  sys.stderr.write("bib: ")
  sys.stderr.write(' '.join(map(str, messages)))
  sys.stderr.write('\n')
  sys.exit(1)


def info(*messages):
  """
  Prints "bib:" and a list of `messages`.
  """
  sys.stderr.write("bib: ")
  sys.stderr.write(' '.join(map(str, messages)))
  sys.stderr.write('\n')


def safe_mkdir(path):
  """
  Creates the directory, including any missing parent directories, at the
  specified `path`.

  Aborts if the path points to an existing regular file.

  Returns the absolute path of the directory.
  """
  if os.path.isfile(path):
    die("'{0}' is a regular file: can't overwrite" % path)
  elif os.path.isdir(path):
    info("directory '%s' already exists" % path)
  else:
    info("creating directory '%s'" % path)
    try:
      os.makedirs(path)
    except OSError as e:
      die("""failed to recursively create the directory
%s
%s
  Do you have write permision to that path?
  Or does part of that path already exist as a regular file?""" % (path, e))
  return os.path.abspath(path)


def checksum(path, method="sha1"):
  """
  Run the checksum `method` on `path`.
  """
  if method == "sha256":
    if BIB_ARCH.startswith("osx"): cmd = ["shasum", "-a", "256"]
    else:                          cmd = ["sha256sum"]
  elif method == "sha1":
    if BIB_ARCH.startswith("osx"): cmd = ["shasum", "-a", "1"]
    else:                          cmd = ["sha1sum"]
  else:
    die("unknown checksum method", method)
  cmd.append(path)
  return subprocess.Popen(cmd,stdout=subprocess.PIPE).communicate()[0].partition(' ')[0]


def fetch(url, path):
  """
  Use curl to downloaded `url` to `path`.
  """
  if os.path.exists(path):
    info("found cached version of", os.path.basename(path))
  else:
    info("fetching", url)
    try:
      subprocess.check_call(['curl', url, '-Lo', path])
    except:
      die("unable to download using curl")

# vim: autoindent expandtab ts=2 shiftwidth=2
