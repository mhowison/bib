# Copyright 2013-2014, Brown University, Providence, RI.
# All Rights Reserved.
#
# This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
# International License. To view a copy of this license, visit
# http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
# Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

import os
import sys
import tempfile

import formula
import utils
from config import *
from errors import *


class Installer(object):

  def __init__(self, formula):
    utils.info("installer init")

    self.formula = formula

    self.prefix = formula.prefix

    self.filename = formula.get("filename", os.path.basename(formula.url))
    self.cachedname = os.path.join(BIB_CACHE, formula.name, self.filename)
    utils.safe_mkdir(os.path.join(BIB_CACHE, formula.name))

    self.download_dir = os.path.join(self.prefix, 'src')

    self.install_sh = tempfile.NamedTemporaryFile('w',
                                            delete=False,
                                            prefix='bib_install_',
                                            suffix='.sh')

  def clean(self):
    """
    Clean up the install prefix, removing any junk from previously failed installs.
    """
    self.install_sh.write("rm -rf %s\n" % self.prefix)


  def fetch(self):
    """
    Check if the file already exists in the cache. If not, download it and
    verify its checksum.
    """
    utils.fetch(self.formula.url, self.cachedname)
    if self.formula.checksum_method:
      checksum = utils.checksum(self.cachedname, self.formula.checksum_method)
      if checksum != self.formula.checksum:
        utils.die(
          "bad checksum for file:", self.cachedname,
          "\n  expecting:", self.formula.checksum,
          "\n      found:", checksum,
          "\nTry deleting this file and rerunning the install command.")


  def decompress(self):
    """
    Decompress the downloaded file intelligently by looking at the file
    extension.
    """
    self.install_sh.write("mkdir -p %s\n" % self.download_dir)
    self.install_sh.write("cd %s\n" % self.download_dir)
    source_dir, ext = os.path.splitext(self.filename)
    if self.filename.endswith('.tar.gz'):
      cmd = "tar xzf %s\n" % self.cachedname
      source_dir = self.filename[:-7]
    elif self.filename.endswith('.tgz'):
      cmd = "tar xzf %s\n" % self.cachedname
    elif self.filename.endswith('.tar.Z'):
      cmd = "tar xzf %s\n" % self.cachedname
      source_dir = self.filename[:-6]
    elif self.filename.endswith('.tar.bz2'):
      cmd = "tar xjf %s\n" % self.cachedname
      source_dir = self.filename[:-8]
    elif self.filename.endswith('.zip'):
      cmd = "unzip %s\n" % self.cachedname
    #elif self.filename.endswith('.jar'):
    else:
      cmd = "mkdir {0}\ncp {1} {0}/\n".format(source_dir, self.cachedname)
    #else:
    #  raise InterfaceException("Error getting decompress command")
    self.install_sh.write(cmd)

    # Set the source and build dirs based on the file that was decompressed.
    if self.formula.source_dir:
      self.source_dir = self.formula.source_dir
    else:
      self.source_dir = source_dir
    self.build_dir = os.path.join(self.download_dir, self.source_dir)


  def executeCommands(self, cmds, workdir):
    if cmds:
      self.install_sh.write("cd %s\n" % workdir)
    for cmd in cmds:
      self.install_sh.write(cmd + "\n")


  def executePrefixCommands(self):
    self.executeCommands(self.formula['prefix_cmds'], self.prefix)


  def executeCustomMakeCommands(self):
    self.executeCommands(self.formula['build_cmds'], self.build_dir)


  def executePostfixCommands(self):
    self.executeCommands(self.formula['postfix_cmds'], self.prefix)


  def writeCleanUpCommands(self):
    self.install_sh.write("rm -rf %s\n" % self.download_dir)


  def runConfigureAndMake(self):
    """
    Runs the configure script.
    """
    pass


  def writeInstallFile(self):
    """
    Penultimate function to write the install script.
    """
    self.install_sh.write("#!/bin/bash\n")
    self.install_sh.write("set -e\n")
    self.install_sh.write("export PATH=%s/bin:$PATH\n" % BIB_DEVKIT)
    self.install_sh.write("export LD_RUN_PATH={0}/lib:{1}/boost155/lib:{1}/gcc48/lib\n".format(BIB_ACTIVE, BIB_DEVKIT))
    self.install_sh.write("export CPATH={0}/include:{1}/boost155/include:{1}/gcc48/include\n".format(BIB_ACTIVE, BIB_DEVKIT))
    self.install_sh.write("export BOOST_ROOT=%s/boost155\n" % BIB_DEVKIT)
    self.install_sh.write("export CC=%s/bin/gcc\n" % BIB_DEVKIT)
    self.install_sh.write("export CXX=%s/bin/g++\n" % BIB_DEVKIT)
    self.clean()
    self.fetch()
    self.decompress()
    self.executePrefixCommands()
    self.runConfigureAndMake()
    self.writeCleanUpCommands()
    self.executePostfixCommands()
    self.install_sh.close()
    utils.info("wrote install script to:", self.install_sh.name)
    return self.install_sh.name


  def writeUninstallFile(self):
    uninstall_sh = tempfile.NamedTemporaryFile('w',
                                            delete=False,
                                            prefix='bib_uninstall_',
                                            suffix='.sh')
    uninstall_sh.write("#!/bin/bash\n")
    uninstall_sh.write("set -e\n")
    uninstall_sh.write("rm -rf %s\n" % self.prefix)
    uninstall_sh.close()
    utils.info("wrote uninstall script to:", uninstall_sh.name)
    return uninstall_sh.name


class InPlaceToolchainInstaller(Installer):
  def __init__(self, formula):
    utils.info("in-place toolchain selected")
    Installer.__init__(self, formula)
    self.download_dir = os.path.dirname(self.prefix)

  def decompress(self):
    Installer.decompress(self)
    self.build_dir = self.prefix
    self.install_sh.write("mv %s %s\n" % (self.source_dir, self.prefix))

  def runConfigureAndMake(self):
    self.executeCustomMakeCommands()

  def writeCleanUpCommands(self):
    pass


class CustomToolchainInstaller(Installer):
  def __init__(self, formula):
    utils.info("custom toolchain selected")
    Installer.__init__(self, formula)

  def runConfigureAndMake(self):
    self.install_sh.write("mkdir -p %s\n" % self.prefix)
    self.executeCustomMakeCommands()


class CMakeToolchainInstaller(Installer):
  def __init__(self, formula):
    utils.info("CMake toolchain selected")
    Installer.__init__(self, formula)

  def runConfigureAndMake(self):
    self.executeCustomMakeCommands()
    self.install_sh.write("cd %s\n" % self.source_dir)
    self.install_sh.write("mkdir build; cd build\n")
    self.install_sh.write("cmake -DCMAKE_INSTALL_PREFIX=%s %s ..\n" % (
                                                  self.prefix,
                                                  self.formula.build_options))
    self.install_sh.write("make\n")
    self.install_sh.write("make install\n")


class GNUToolchainInstaller(Installer):
  def __init__(self, formula):
    utils.info("GNU toolchain selected")
    Installer.__init__(self, formula)

  def runConfigureAndMake(self):
    self.install_sh.write("cd %s\n" % self.source_dir)
    self.install_sh.write("./configure --prefix=%s %s\n" % (
                                                  self.prefix,
                                                  self.formula.build_options))
    self.install_sh.write("make\n")
    self.install_sh.write("make install\n")
    self.executeCustomMakeCommands()


def InstallerFactory(formula):
  """
  Return the appropriate installer object depending on the toolchain specified
  in the formula.
  """

  if formula.toolchain == 'inplace':
    return InPlaceToolchainInstaller(formula)

  elif formula.toolchain == 'custom':
    return CustomToolchainInstaller(formula)

  elif formula.toolchain == 'cmake':
    return CMakeToolchainInstaller(formula)

  elif formula.toolchain == 'gnu':
    return GNUToolchainInstaller(formula)

  else:
    utils.die("unknown toolchain:", formula.toolchain)


# vim: autoindent expandtab ts=2 shiftwidth=2
