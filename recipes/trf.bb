[default]
name = trf
homepage = http://tandem.bu.edu/trf/trf.html
toolchain = inplace
build_cmds = mv trf.download.pl* trf; chmod a+x trf
bin = trf

[20121126_v_4.07b_v_centos]
url = http://tandem.bu.edu/cgi-bin/trf/trf.download.pl?fileselect=26
sha1 = b4f24766f27f3b5f02957fa565da3bd114c37f9a

[20121126_v_4.07b_v_ubuntu]
url = http://tandem.bu.edu/cgi-bin/trf/trf.download.pl?fileselect=26
sha1 = b4f24766f27f3b5f02957fa565da3bd114c37f9a

[20121126_v_4.07b_v_osx]
url = http://tandem.bu.edu/cgi-bin/trf/trf.download.pl?fileselect=29
sha1 = f25531945eb072aa2176f56222f2f26a3a82d0b0
