[default]
name = velvet
homepage = http://www.ebi.ac.uk/~zerbino/velvet/
toolchain = inplace
build_cmds = make CC=gcc OPENMP=1 MAXKMERLENGTH=127 LONGSEQUENCES=1 velveth velvetg
bin = velveth velvetg

[20141111_v_1.2.10_v_ALL]
url = https://www.ebi.ac.uk/~zerbino/velvet/velvet_1.2.10.tgz
sha1 = 216f0941609abf3a73adbba19ef1f364df489d18

[20140116_v_1.2.09_v_ALL]
url = http://www.ebi.ac.uk/~zerbino/velvet/velvet_1.2.09.tgz
sha1 = e40d59806599ac06e4b13408205b8978427177d0
