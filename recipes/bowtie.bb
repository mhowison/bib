[default]
name = bowtie
homepage = http://bowtie-bio.sourceforge.net
toolchain = inplace
build_cmds = sed -i~ -e 's/-stdlib=libstdc++//' Makefile; make CXX=g++ CXXFLAGS=-m64
bin = bowtie bowtie-build bowtie-inspect

[20150623_v_1.1.2_v_ALL]
url = http://downloads.sourceforge.net/project/bowtie-bio/bowtie/1.1.2/bowtie-1.1.2-src.zip
source_dir = bowtie-1.1.2
sha256 = b1e9ccc825207efd1893d9e33244c681bcb89b9b2b811eb95a9f5a92eab637ae

[20130409_v_1.0.0_v_ALL]
url = http://downloads.sourceforge.net/project/bowtie-bio/bowtie/1.0.0/bowtie-1.0.0-src.zip
source_dir = bowtie-1.0.0
sha256 = 51e434a78e053301f82ae56f4e94f71f97b19f7175324777a7305c8f88c5bac5
