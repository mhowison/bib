[default]
name = last
homepage = http://last.cbrc.jp
toolchain = inplace
build_cmds = make install prefix=%(prefix)s
bin = bin/*

[20140723_v_465_v_ALL]
url = http://last.cbrc.jp/last-465.zip
sha1 = 836b5902a13e3915c9160fa41f0719f7efaa5b16
