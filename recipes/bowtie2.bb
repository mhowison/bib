[default]
name = bowtie2
homepage = http://bowtie-bio.sourceforge.net/bowtie2
toolchain = custom
build_cmds = make CC=gcc CXX=g++ BOWTIE_PTHREADS=1; cp bowtie2 bowtie2-* %(prefix)s
bin = bowtie2 bowtie2-*

[20140530_v_2.2.3_v_ALL]
url = http://downloads.sourceforge.net/project/bowtie-bio/bowtie2/2.2.3/bowtie2-2.2.3-source.zip
source_dir = bowtie2-2.2.3

[20130221_v_2.1.0_v_ALL]
url = http://downloads.sourceforge.net/project/bowtie-bio/bowtie2/2.1.0/bowtie2-2.1.0-source.zip
source_dir = bowtie2-2.1.0
sha256 = 90a9d3a6bd19ddc3a8f90b935c6a2288478572de2ad4039b29f91016b95ef4b0

[20130127_v_2.0.6_v_ALL]
url = http://downloads.sourceforge.net/project/bowtie-bio/bowtie2/2.0.6/bowtie2-2.0.6-source.zip
source_dir = bowtie2-2.0.6
sha256 = e06d983a0a3a309c3226fa18b98cc9c35a7f8693e636a598e818b331aaabc1b6
