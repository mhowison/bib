[default]
name = zlib
homepage = http://zlib.net
toolchain = gnu
include = include/*
lib = lib/*
lib/pkgconfig = lib/pkgconfig/zlib.pc
man/man3 = share/man/man3/*

[20130428_v_1.2.8_v_ALL]
url = http://zlib.net/zlib-1.2.8.tar.gz
md5 = 44d667c142d7cda120332623eab69f40
