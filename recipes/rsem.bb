[default]
name = rsem
homepage = http://deweylab.biostat.wisc.edu/rsem/
toolchain = inplace
build_cmds = make
bin = samtools convert-sam-for-rsem extract-transcript-to-gene-map-from-trinity rsem-* rsem_perl_utils.pm
system-depends = +curses/any

[20150727_v_1.2.22_v_ALL]
url = http://deweylab.biostat.wisc.edu/rsem/src/rsem-1.2.22.tar.gz
sha1 = 53a174a65f675949f4be1bc5943bdd7ceba7b514

[20140616_v_1.2.15_v_ALL]
url = http://deweylab.biostat.wisc.edu/rsem/src/rsem-1.2.15.tar.gz
sha1 = 95f442f68368b74aa109437ea99c552b5127b155

[20140526_v_1.2.13_v_ALL]
url = http://deweylab.biostat.wisc.edu/rsem/src/rsem-1.2.13.tar.gz
sha1 = c76e308dd39b2bc8705a498e5d57f42d22d02c1a

[20130108_v_1.2.9_v_ALL]
url = http://deweylab.biostat.wisc.edu/rsem/src/rsem-1.2.9.tar.gz
sha1 = 0ce9ca67229ca831629df676040d5fe3cab01c11
