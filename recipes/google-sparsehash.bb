[default]
name = google-sparsehash
homepage = http://code.google.com/p/google-sparsehash/
toolchain = gnu
build_options = --disable-dependency-tracking
include = include/google include/sparsehash
lib/pkgconfig = lib/pkgconfig/libsparsehash.pc
system-depends = =g++/any

[20120223_v_2.0.2_v_ALL]
url = http://sparsehash.googlecode.com/files/sparsehash-2.0.2.tar.gz
sha1 = 12c7552400b3e20464b3362286653fc17366643e
