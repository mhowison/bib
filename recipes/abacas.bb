[default]
name = abacas
homepage = http://abacas.sourceforge.net
depends = *mummer/*
toolchain = inplace
build_cmds = mv abacas.*.pl abacas; chmod a+x abacas; sed -i -e '1 s|usr/local/bin/perl|usr/bin/env perl|' abacas
bin = abacas

[20101103_v_1.3.1_v_ALL]
url = http://downloads.sourceforge.net/project/abacas/abacas.1.3.1.pl
sha1 = 495ef27a4a4ed5ad878bb32f6d2080a53a5ab7cf
