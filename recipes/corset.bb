[default]
name = corset
homepage = https://github.com/Oshlack/Corset/wiki
toolchain = gnu
build_options = --with-bam_inc=%(BIB_INSTALL)s/samtools/1.2/include/bam --with-bam_lib=%(BIB_INSTALL)s/samtools/1.2/lib LIBS=%(BIB_INSTALL)s/htslib/1.2.1/lib/libhts.a
depends = +samtools/1.2,+htslib/1.2.1
bin = bin/corset

[20141221_v_1.04_v_ALL]
url = https://github.com/Oshlack/Corset/archive/version-1.04.tar.gz
sha1 = d83b48fc643eeccd1229d9ae5ab0f8980149923b
source_dir = Corset-version-1.04
