[default]
name = trinity
homepage = https://trinityrnaseq.github.io
toolchain = inplace
postfix_cmds = sed -e 's/FindBin::Bin/FindBin::RealBin/g' -i~ Trinity
bin = Trinity

[20151015_v_2.1.1_v_ALL]
url = https://github.com/trinityrnaseq/trinityrnaseq/archive/v2.1.1.tar.gz
sha1 = 5b537b8f8c35c6dd2853c9d82c761e6efd35c228
prefix_cmds = sed -e 's/ LIBPATH=-ltinfo//' -i~ trinity-plugins/Makefile
source_dir = trinityrnaseq-2.1.1
build_cmds = make CXXFLAGS=-fopenmp

[20150302_v_2.0.6_v_ALL]
url = https://github.com/trinityrnaseq/trinityrnaseq/archive/v2.0.6.tar.gz
sha1 = b67d5d272e33265b35d4dd68cd96cb816f1ac6b8
source_dir = trinityrnaseq-2.0.6
build_cmds = make CXXFLAGS=-fopenmp

[20140717_v_r20140717_v_ALL]
url = http://downloads.sourceforge.net/trinityrnaseq/trinityrnaseq_r20140717.tar.gz
sha1 = fd559efe2005fb0c568b280a3edf43e25e6e6aba
build_cmds = make -B CXXFLAGS=-fopenmp

[20140423_v_r20140413p1_v_ALL]
url = http://downloads.sourceforge.net/trinityrnaseq/trinityrnaseq_r20140413p1.tar.gz
sha1 = d916cc7099a6a45de923e20bba85bb7949420395
build_cmds = make -B CXXFLAGS=-fopenmp

[20130814_v_r2013_08_14_v_ALL]
url = http://downloads.sourceforge.net/trinityrnaseq/trinityrnaseq_r2013_08_14.tgz
sha1 = 3ca275e79e7740974a949a7fbe469e2e74471e3f
build_cmds = make -B CXXFLAGS=-fopenmp
bin = Trinity.pl Inchworm/bin/inchworm Chrysalis/Chrysalis Chrysalis/QuantifyGraph util/partition_chrysalis_graphs_n_reads.pl Butterfly/Butterfly.jar
