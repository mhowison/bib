[default]
name = raxml
homepage = https://github.com/stamatak/standard-RAxML
toolchain = custom
build_cmds = make -f Makefile.SSE3.PTHREADS.gcc CC=gcc CFLAGS=-pthread; make -f Makefile.SSE3.gcc CC=gcc CFLAGS=-pthread; cp raxmlHPC-PTHREADS-SSE3 raxmlHPC-SSE3 %(prefix)s
bin = raxmlHPC-PTHREADS-SSE3 raxmlHPC-SSE3

[20150709_v_8.2.0_v_ALL]
url = https://github.com/stamatak/standard-RAxML/archive/v8.2.0.zip
source_dir = standard-RAxML-8.2.0
sha1 = ae975624886fa5b5705b09fc00c52ca1ccfc57e3

[20130829_v_7.7.6_v_ALL]
url = https://github.com/stamatak/standard-RAxML/archive/v7.7.6.zip
source_dir = standard-RAxML-7.7.6
sha1 = a7e46408a01f7eea00394e3659791b1826c5f195
