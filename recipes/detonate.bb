[default]
name = detonate
homepage = http://deweylab.biostat.wisc.edu/detonate/
toolchain = inplace
depends = *rsem/*
build_cmds = sed -e 's|cd cmake && $(MAKE)|touch cmake/finished|' -i~ ref-eval/Makefile; sed -e 's|../../cmake/install/bin/cmake|cmake|' -i~ ref-eval/lemon/Makefile; make
bin = ref-eval/ref-eval ref-eval/ref-eval-estimate-true-assembly rsem-eval/rsem-eval*

[20150901_v_1.10_v_ALL]
url = http://deweylab.biostat.wisc.edu/detonate/detonate-1.10.tar.gz
sha1 = 4e06bbae7d5a0a40101c5aa618c47fbc363de94b
