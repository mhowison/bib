[default]
name = fasta
homepage = http://fasta.bioch.virginia.edu
toolchain = inplace
build_cmds = cd src; make -f ../make/Makefile.linux64_sse2 all
bin = bin/*

[20140129_v_36.3.6d_v_ALL]
url = http://faculty.virginia.edu/wrpearson/fasta/fasta36/fasta-36.3.6d.tar.gz
sha1 = 54e08a52de4e11600600c224fbd341eb668d3b0a
