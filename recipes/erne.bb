[default]
name = erne
homepage = http://erne.sourceforge.net
bin = bin/*

[20140417_v_1.4.3_v_centos]
url = http://downloads.sourceforge.net/project/erne/1.4.3/erne-1.4.3-linux.tar.gz
sha1 = 8834daa00a9b782a61420f55c21e115d4fc4d44f
toolchain = inplace

[20140417_v_1.4.3_v_ubuntu]
url = http://downloads.sourceforge.net/project/erne/1.4.3/erne-1.4.3-linux.tar.gz
sha1 = 8834daa00a9b782a61420f55c21e115d4fc4d44f
toolchain = inplace

[20140417_v_1.4.3_v_osx]
url = http://downloads.sourceforge.net/project/erne/1.4.3/erne-1.4.3-source.tar.gz
sha1 = d420c1707f95911a9f8a5d399fa5af6b87ac99b6
toolchain = custom
build_cmds = sed -e "s/^x86_64\*)/*)/" -e "s/-Wl,-R/-Wl,-rpath -Wl,/" configure >configure-fixed; sh configure-fixed --prefix=%(prefix)s; make install
