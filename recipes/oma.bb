[default]
name = oma
homepage = http://omabrowser.org/standalone
toolchain = inplace
bin = bin/oma

[20140922_v_0.99x_v_ALL]
url = http://omabrowser.org/standalone/OMA.0.99x.tgz
source_dir = OMA.0.99x
sha1 = 572ad20bbd291fd8203f0cc774d7d6bdd6e36637

[20131201_v_0.99u.3_v_ALL]
url = http://omabrowser.org/standalone/OMA.0.99u.3.tgz
source_dir = OMA.0.99u.3
sha1 = 47dd1d16d1768612e372829e809dc1c4f7d46b20
