[default]
name = blast
homepage = http://blast.ncbi.nlm.nih.gov
toolchain = inplace
bin = bin/*

[20150615_v_2.2.31+_v_centos]
url = ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.2.31/ncbi-blast-2.2.31+-x64-linux.tar.gz
source_dir = ncbi-blast-2.2.31+
md5 = ae789a5bfb523cef95ac40e5a6b1929e

[20150615_v_2.2.31+_v_ubuntu]
url = ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.2.31/ncbi-blast-2.2.31+-x64-linux.tar.gz
source_dir = ncbi-blast-2.2.31+
md5 = ae789a5bfb523cef95ac40e5a6b1929e

[20150615_v_2.2.31+_v_osx]
url = ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.2.31/ncbi-blast-2.2.31+-universal-macosx.tar.gz
source_dir = ncbi-blast-2.2.31+
md5 = 316555fef19749fe90455b7def03634e

[20130106_v_2.2.29+_v_centos]
url = ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.2.29/ncbi-blast-2.2.29+-x64-linux.tar.gz
source_dir = ncbi-blast-2.2.29+
sha1 = ffa0dad3f2cbe294d6a6f34f73c79663743dccd9

[20130106_v_2.2.29+_v_ubuntu]
url = ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.2.29/ncbi-blast-2.2.29+-x64-linux.tar.gz
source_dir = ncbi-blast-2.2.29+
sha1 = ffa0dad3f2cbe294d6a6f34f73c79663743dccd9

[20130106_v_2.2.29+_v_osx]
url = ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.2.29/ncbi-blast-2.2.29+-universal-macosx.tar.gz
source_dir = ncbi-blast-2.2.29+
sha1 = 89b7722f9be47a882b23b50bc16b3a7bf9c68eaa
