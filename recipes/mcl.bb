[default]
name = mcl
homepage = http://micans.org/mcl
toolchain = gnu
bin = bin/*
man/man1 = share/man/man1/*
man/man5 = share/man/man5/*
man/man7 = share/man/man7/*
cleanup_after_install = true

[20131201_v_12-135_v_ALL]
url = http://micans.org/mcl/src/mcl-12-135.tar.gz
source_dir = mcl-12-135
sha1 = 27e7bc08fe5f0d3361bbc98d343c9d045712e406
