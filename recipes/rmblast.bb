[default]
name = rmblast
homepage = http://www.repeatmasker.org/RMBlast.html
toolchain = inplace
bin = bin/rmblastn

[20130227_v_2.2.28_v_centos]
url = ftp://ftp.ncbi.nlm.nih.gov/blast/executables/rmblast/2.2.28/ncbi-rmblastn-2.2.28-x64-linux.tar.gz
source_dir = ncbi-rmblastn-2.2.28
sha1 = 02abee397973ccdf7d68e161203bd8dbe9322a6c

[20130227_v_2.2.28_v_ubuntu]
url = ftp://ftp.ncbi.nlm.nih.gov/blast/executables/rmblast/2.2.28/ncbi-rmblastn-2.2.28-x64-linux.tar.gz
source_dir = ncbi-rmblastn-2.2.28
sha1 = 02abee397973ccdf7d68e161203bd8dbe9322a6c

[20130227_v_2.2.28_v_osx]
url = ftp://ftp.ncbi.nlm.nih.gov/blast/executables/rmblast/2.2.28/ncbi-rmblastn-2.2.28-universal-macosx.tar.gz
source_dir = ncbi-rmblastn-2.2.28
sha1 = a8f74b034d88c3e2202dd9a4dd30fdb78161aa75
