[default]
name = mafft
homepage = http://mafft.cbrc.jp/alignment/software
toolchain = custom
bin = bin/*
man/man1 = man/man1/*

[20131211_v_7.130_v_ALL]
url = http://mafft.cbrc.jp/alignment/software/mafft-7.130-without-extensions-src.tgz
source_dir = mafft-7.130-without-extensions/core
build_cmds = LDFLAGS=-lgcc_eh make PREFIX=%(prefix)s MANDIR=%(prefix)s/man/man1 install
sha1 = 05361247f57e5ab10a268d6eef4982accf13b42e
