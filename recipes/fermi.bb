[default]
name = fermi
homepage = https://github.com/lh3/fermi
toolchain = inplace
build_cmds = make
bin = fermi run-fermi.pl
man/man1 = fermi.1

[20140410_v_1.1_v_ALL]
url = https://github.com/downloads/lh3/fermi/fermi-1.1.tar.bz2
sha1 = 9b725465b1c56e5168050c85ad548a564dcba306
