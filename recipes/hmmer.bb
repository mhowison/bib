[default]
name = hmmer
homepage = http://hmmer.janelia.org/
toolchain = gnu
bin = bin/*

[20150305_v_3.1b2_v_ALL]
url = http://eddylab.org/software/hmmer3/3.1b2/hmmer-3.1b2.tar.gz
md5 = c8c141018bc0ccd7fc37b33f2b945d5f
build_cmds = cd easel; make install

[20140117_v_3.0_v_ALL]
url = http://eddylab.org/software/hmmer3/3.0/hmmer-3.0.tar.gz
sha256 = 6977e6473fcb554b1d5a86dc9edffffa53918c1bd88d7fd20d7499f1ba719e83
