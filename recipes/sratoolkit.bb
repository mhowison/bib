[default]
name = sratoolkit
homepage = https://github.com/ncbi/sra-tools/wiki
toolchain = inplace
bin = bin/*[a-z]

[20150918_v_2.5.4_v_centos]
url = http://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.5.4/sratoolkit.2.5.4-centos_linux64.tar.gz
md5 = 98d1b06f4d8e3162a09c75f53387df43

[20150918_v_2.5.4_v_ubuntu]
url = http://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.5.4/sratoolkit.2.5.4-ubuntu64.tar.gz
md5 = d762e076d3a8bffbe157e84b5695f7f5

[20150918_v_2.5.4_v_osx]
url = http://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.5.4/sratoolkit.2.5.4-mac64.tar.gz
md5 = 3f47d7e5a19755ffea1823a5391c9625

[20131202_v_0.2.3.4-2_v_centos]
url = http://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.3.4-2/sratoolkit.2.3.4-2-centos_linux64.tar.gz
sha1 = 3ed1bfd249e47c77544be7be6fa15b99b1625f87

[20131202_v_0.2.3.4-2_v_ubuntu]
url = http://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.3.4-2/sratoolkit.2.3.4-2-ubuntu64.tar.gz
sha1 = 3ed1bfd249e47c77544be7be6fa15b99b1625f87

[20131202_v_0.2.3.4-2_v_osx]
url = http://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.3.4-2/sratoolkit.2.3.4-2-mac64.tar.gz
sha1 = 35c9e93fd5f2c22dd92393562d52d7e345cdc20a
