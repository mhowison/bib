[default]
name = shrimp
homepage = http://compbio.cs.toronto.edu/shrimp/
toolchain = inplace
build_cmds = sed -i~ -e 's/-lstdc++$/-lgcc_eh/' Makefile; make CXX=g++
bin = bin/*

[20120701_v_2.2.3_v_ALL]
url = http://compbio.cs.toronto.edu/shrimp/releases/SHRiMP_2_2_3.src.tar.gz
source_dir = SHRiMP_2_2_3
sha1 = 86623037959d76118a7eb8f7af0a9c27b2d242a1
