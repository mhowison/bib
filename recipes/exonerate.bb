[default]
name = exonerate
homepage = http://www.ebi.ac.uk/~guy/exonerate
toolchain = inplace
bin = bin/*
man/man1 = man/man1/*

[20081030_v_2.2.0_v_centos]
url = http://www.ebi.ac.uk/~guy/exonerate/exonerate-2.2.0-x86_64.tar.gz
sha1 = 277ee496fa4cb6ad3cc375db1f7315a7c8908f2d

[20081030_v_2.2.0_v_ubuntu]
url = http://www.ebi.ac.uk/~guy/exonerate/exonerate-2.2.0-x86_64.tar.gz
sha1 = 277ee496fa4cb6ad3cc375db1f7315a7c8908f2d
