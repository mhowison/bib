[default]
name = htslib
homepage = http://www.htslib.org
toolchain = gnu
bin = bin/*
include = include/htslib
lib = lib/*
lib/pkgconfig = lib/pkgconfig/htslib.pc
man/man1 = share/man/man1/*
man/man5 = share/man/man5/*

[20150203_v_1.2.1_v_ALL]
url = https://github.com/samtools/htslib/releases/download/1.2.1/htslib-1.2.1.tar.bz2
sha1 = 573109011607b53e35aff211d5825d15a439cee9
