[default]
name = samtools
homepage = http://sourceforge.net/projects/samtools/
toolchain = custom
build_cmds = make prefix=%(prefix)s install; mkdir -p %(prefix)s/lib; cp libbam.a %(prefix)s/lib; mkdir -p %(prefix)s/include/bam; cp *.h %(prefix)s/include/bam/
bin = bin/*
lib = lib/libbam.a
include = include/bam
man/man1 = share/man/man1/samtools.1

[20150203_v_1.2_v_ALL]
url = https://github.com/samtools/samtools/releases/download/1.2/samtools-1.2.tar.bz2
sha1 = 57b7c9b048c1081f6d5f7955c10724f6065380bd

[20130319_v_0.1.19_v_ALL]
url = http://downloads.sourceforge.net/project/samtools/samtools/0.1.19/samtools-0.1.19.tar.bz2
sha1 = ff3f4cf40612d4c2ad26e6fcbfa5f8af84cbe881
toolchain = inplace
build_cmds = make; make razip; make -C bcftools
postfix_cmds = mkdir bam; mv *.h bam
bin = samtools razip bcftools/bcftools bcftools/vcfutils.pl misc/maq2sam-long misc/maq2sam-short misc/md5fa misc/md5sum-lite misc/wgsim misc/*.pl
lib = libbam.a
include = bam
