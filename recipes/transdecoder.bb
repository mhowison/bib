[default]
name = transdecoder
homepage = http://transdecoder.sourceforge.net/
toolchain = inplace

[20150127_v_2.0.1_v_ALL]
url = https://github.com/TransDecoder/TransDecoder/archive/2.0.1.tar.gz
source_dir = TransDecoder-2.0.1
sha1 = 1a36aa7ecb3af9e984eb9bee818871093fa1ab74
build_cmds = make
bin = TransDecoder.*

[20120815_v_r2012-08-15_v_ALL]
url = http://downloads.sourceforge.net/project/transdecoder/OLDER/transdecoder_r2012-08-15.tgz
sha1 = 9b7dedb327761a2c328770fd53eaafd6b29b9ffa
postfix_cmds = sed -e 's/FindBin::Bin/FindBin::RealBin/g' -i *.pl
bin = *.pl
