[default]
name = gsl
homepage = http://www.gnu.org/software/gsl/
toolchain = gnu
bin = bin/*
include = include/gsl
lib = lib/*
lib/pkgconfig = lib/pkgconfig/gsl.pc
man/man1 = share/man/man1/*
man/man3 = share/man/man3/*

[20130719_v_1.16_v_ALL]
url = http://ftpmirror.gnu.org/gsl/gsl-1.16.tar.gz
sha1 = 210af9366485f149140973700d90dc93a4b6213e
