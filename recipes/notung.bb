[default]
name = notung
homepage = http://www.cs.cmu.edu/~durand/Notung
toolchain = inplace
system-depends = +java/any

[20080723_v_2.6_v_ALL]
url = http://lampetra.compbio.cs.cmu.edu/Notung/distributions/Notung-2.6.zip
sha1 = cf69bbd1f0a50e195b26c9bc7891842c1efaeed0
postfix_cmds = echo -e "#!/bin/sh\njava -jar \$(which Notung-2.6.jar) \"\$@\"" >notung; chmod a+x notung Notung-2.6.jar
bin = notung Notung-2.6.jar
