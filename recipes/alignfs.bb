[default]
name = alignfs
homepage = https://github.com/LooseLab/AlignFS
toolchain = inplace
depends = *muscle/* *blast/*
bin = AlignFS.pl

[20140930_v_20140930_v_ALL]
url = https://github.com/LooseLab/AlignFS/archive/2605cd79b93a76c6df9c18659e10581854b40d47.zip
source_dir = AlignFS-2605cd79b93a76c6df9c18659e10581854b40d47
sha1 = 92296e67b492f99b051a080096b1d1a4f1e3a9b6
