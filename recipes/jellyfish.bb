[default]
name = jellyfish
homepage = http://www.genome.umd.edu/jellyfish.html
toolchain = gnu
bin = bin/*
lib = lib/*
lib/pkgconfig = lib/pkgconfig/*
man/man1 = share/man/man1/*

[20150701_v_2.2.3_v_ALL]
url = https://github.com/gmarcais/Jellyfish/releases/download/v2.2.3/jellyfish-2.2.3.tar.gz
sha1 = ca4acaf04ba70e4a8cf2a3e73f26a4c21ee099a4
include = include/jellyfish-2.2.3/jellyfish

[20140116_v_1.1.11_v_ALL]
url = http://www.cbcb.umd.edu/software/jellyfish/jellyfish-1.1.11.tar.gz
sha1 = 8bd6a7b382e94d37adfddd916dec2b0e36f840fd
include = include/jellyfish-1.1.11/jellyfish
