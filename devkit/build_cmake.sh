#!/bin/sh

set -e

MAKEJ=${MAKEJ-"2"}
UNAME=$(uname)
BIN=/opt/bib/devkit/bin
echo "MAKEJ=$MAKEJ"
echo "UNAME=$UNAME"

mkdir -p /opt/bib/devkit/src
cd /opt/bib/devkit/src

if [ "$UNAME" == "Darwin" ]; then
  curl -LO http://www.cmake.org/files/v2.8/cmake-2.8.12.1.tar.gz
  tar xf cmake-2.8.12.1.tar.gz
  cd cmake-2.8.12.1
  BIN=/opt/bib/devkit/bin
  CC=$BIN/gcc CXX=$BIN/g++ FC=$BIN/gfortran ./bootstrap --prefix=/opt/bib/devkit/cmake28 --system-libs --no-system-libarchive
  make install
  cd ../../bin
  ln -s ../cmake28/bin/cmake .
fi
