#!/bin/bash

set -e

wget http://ftpmirror.gnu.org/gcc/gcc-5.2.0/gcc-5.2.0.tar.bz2
wget https://gmplib.org/download/gmp/gmp-6.0.0a.tar.xz
wget ftp://ftp.gnu.org/gnu/mpc/mpc-1.0.3.tar.gz
wget http://www.mpfr.org/mpfr-current/mpfr-3.1.4.tar.xz

tar xf gcc-5.2.0.tar.bz2

tar xf gmp-6.0.0a.tar.xz
mv gmp-6.0.0 gcc-5.2.0/gmp
 
tar xf mpc-1.0.3.tar.gz
mv mpc-1.0.3 gcc-5.2.0/mpc
 
tar xf mpfr-3.1.4.tar.xz
mv mpfr-3.1.4 gcc-5.2.0/mpfr
 
rm -rf gcc-5.2.0/gcc-build
mkdir gcc-5.2.0/gcc-build
cd gcc-5.2.0/gcc-build
 
../configure --prefix=/gpfs/runtime/opt/gcc/5.2.0 \
             --disable-multilib \
             --enable-languages=c,c++,fortran \
             --enable-libstdcxx-threads \
             --enable-libstdcxx-time \
             --enable-shared \
             --enable-__cxa_atexit \
             --disable-libunwind-exceptions \
             --disable-libada \
             --host x86_64-redhat-linux-gnu \
             --build x86_64-redhat-linux-gnu
 
make -j4
 
make install
