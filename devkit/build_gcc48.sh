#!/bin/sh

set -e

PREFIX=/opt/bib/devkit/gcc48
MAKEJ=${MAKEJ-"2"}
UNAME=$(uname)
echo "MAKEJ=$MAKEJ"
echo "UNAME=$UNAME"

mkdir -p /opt/bib/devkit/src
cd /opt/bib/devkit/src

curl -LO http://ftpmirror.gnu.org/gcc/gcc-4.8.5/gcc-4.8.5.tar.bz2
curl -LO http://ftpmirror.gnu.org/gmp/gmp-4.3.2.tar.bz2
curl -LO http://www.mpfr.org/mpfr-2.4.2/mpfr-2.4.2.tar.bz2
curl -LO http://multiprecision.org/mpc/download/mpc-0.8.1.tar.gz
curl -LO ftp://gcc.gnu.org/pub/gcc/infrastructure/cloog-0.18.0.tar.gz
curl -LO http://isl.gforge.inria.fr/isl-0.11.1.tar.bz2

tar xf gmp-4.3.2.tar.bz2
cd gmp-4.3.2
# Patches gmp.h to remove the __need_size_t define, which
# was preventing libc++ builds from getting the ptrdiff_t type
# Applied upstream in http://gmplib.org:8000/gmp/raw-rev/6cd3658f5621
echo "diff -r c7ed424a63b2 -r 6cd3658f5621 gmp-h.in
--- a/gmp-h.in	Tue Oct 08 14:01:35 2013 +0200
+++ b/gmp-h.in	Tue Oct 08 14:45:27 2013 +0200
@@ -46,13 +46,11 @@
 #ifndef __GNU_MP__
 #define __GNU_MP__ 5
 
-#define __need_size_t  /* tell gcc stddef.h we only want size_t */
 #if defined (__cplusplus)
 #include <cstddef>     /* for size_t */
 #else
 #include <stddef.h>    /* for size_t */
 #endif
-#undef __need_size_t
 
 /* Instantiated by configure. */
 #if ! defined (__GMP_WITHIN_CONFIGURE)
" >patch.txt
patch -p 1 < patch.txt
if [ "$UNAME" == "Darwin" ]; then
  BUILD="--build=x86_64-apple-darwin"
fi
./configure --prefix=$PREFIX --enable-cxx $BUILD --disable-shared
make -j$MAKEJ
make check
make install
cd ..

tar xf mpfr-2.4.2.tar.bz2
cd mpfr-2.4.2
./configure --prefix=$PREFIX --disable-dependency-tracking --with-gmp=$PREFIX --build=x86_64-apple-darwin --disable-shared
make -j$MAKEJ
make install
cd ..

tar xf mpc-0.8.1.tar.gz
cd mpc-0.8.1
./configure --prefix=$PREFIX --disable-dependency-tracking --with-gmp=$PREFIX --with-mpfr=$PREFIX --disable-shared
make -j$MAKEJ
make install
cd ..

tar xf isl-0.11.1.tar.bz2
cd isl-0.11.1
./configure --prefix=$PREFIX --disable-dependency-tracking --disable-silent-rules --with-gmp=system --with-gmp-prefix=$PREFIX --disable-shared
make -j$MAKEJ
make install
cd ..

tar xf cloog-0.18.0.tar.gz
cd cloog-0.18.0
./configure --prefix=$PREFIX --disable-dependency-tracking --disable-silent-rules --with-gmp-prefix=$PREFIX --with-isl-prefix=$PREFIX --disable-shared
make -j$MAKEJ
make install
cd ..

tar xf gcc-4.8.5.tar.bz2
cd gcc-4.8.5
if [ "$UNAME" == "Darwin" ]; then
  patch -p0 < ../../patch_gcc48_define_non_standard_clang_macros.txt
  patch -p1 < ../../patch_gcc48_dont-mark-objc-locals-no-dead-strip.txt
fi
mkdir build
cd build
if [ "$UNAME" == "Darwin" ]; then
  BUILD="--build=x86_64-apple-darwin15.4.0"
fi
../configure $BUILD --prefix=$PREFIX --enable-languages=c,c++,fortran --with-gmp=$PREFIX --with-mpfr=$PREFIX --with-mpc=$PREFIX --with-cloog=$PREFIX --with-isl=$PREFIX --with-system-zlib --enable-libstdcxx-time=yes --enable-stage1-checking --enable-checking=release --enable-lto --enable-plugin
make -j$MAKEJ bootstrap
make install
cd ../..

mkdir -p /opt/bib/devkit/bin
cd /opt/bib/devkit/bin
ln -s ../gcc48/bin/gcc .
ln -s ../gcc48/bin/g++ .
ln -s ../gcc48/bin/gfortran .
