#!/bin/sh

set -e

PREFIX=/opt/bib/devkit/gcc44
MAKEJ=${MAKEJ-"2"}
UNAME=$(uname)
echo "MAKEJ=$MAKEJ"
echo "UNAME=$UNAME"

mkdir -p /opt/bib/devkit/src
cd /opt/bib/devkit/src

curl -LO ftp://ftp.gmplib.org/pub/gmp-4.3.2/gmp-4.3.2.tar.bz2
curl -LO http://www.mpfr.org/mpfr-2.4.2/mpfr-2.4.2.tar.bz2
curl -LO http://bugseng.com/products/ppl/download/ftp/releases/0.11/ppl-0.11.tar.gz
curl -LO ftp://gcc.gnu.org/pub/gcc/infrastructure/cloog-ppl-0.15.11.tar.gz
curl -LO http://ftpmirror.gnu.org/gcc/gcc-4.4.7/gcc-4.4.7.tar.bz2

tar xf gmp-4.3.2.tar.bz2
cd gmp-4.3.2
# Patches gmp.h to remove the __need_size_t define, which
# was preventing libc++ builds from getting the ptrdiff_t type
# Applied upstream in http://gmplib.org:8000/gmp/raw-rev/6cd3658f5621
echo "diff -r c7ed424a63b2 -r 6cd3658f5621 gmp-h.in
--- a/gmp-h.in	Tue Oct 08 14:01:35 2013 +0200
+++ b/gmp-h.in	Tue Oct 08 14:45:27 2013 +0200
@@ -46,13 +46,11 @@
 #ifndef __GNU_MP__
 #define __GNU_MP__ 5
 
-#define __need_size_t  /* tell gcc stddef.h we only want size_t */
 #if defined (__cplusplus)
 #include <cstddef>     /* for size_t */
 #else
 #include <stddef.h>    /* for size_t */
 #endif
-#undef __need_size_t
 
 /* Instantiated by configure. */
 #if ! defined (__GMP_WITHIN_CONFIGURE)
" >patch.txt
patch -p 1 < patch.txt
if [ "$UNAME" == "Darwin" ]; then
  BUILD="--build=x86_64-apple-darwin"
fi
./configure --prefix=$PREFIX --enable-cxx $BUILD --disable-static
make -j$MAKEJ
make check
make install
cd ..

tar xf mpfr-2.4.2.tar.bz2
cd mpfr-2.4.2
./configure --prefix=$PREFIX --disable-dependency-tracking --with-gmp=$PREFIX --build=x86_64-apple-darwin --disable-static
make -j$MAKEJ
make install
cd ..

tar xf ppl-0.11.tar.gz
cd ppl-0.11
# Patch for "./Row.defs.hh:503:15: error: flexible array member 'vec_' of type 'Coefficient []' with non-trivial destruction"
# https://gist.githubusercontent.com/manphiz/9507743/raw/45081e12c2f1faf81e8536f365af05173c6dab5c/patch-ppl-flexible-array-clang_v2.patch
echo "diff -urN ppl-0.11.orig/configure ppl-0.11/configure
--- ppl-0.11.orig/configure	2010-08-02 13:21:43.000000000 -0700
+++ ppl-0.11/configure	2014-03-12 07:11:03.000000000 -0700
@@ -9134,6 +9134,8 @@
   A()
     : i(0), b(false) {
   }
+
+  ~A() {}
 };
 
 class B {
@@ -9193,6 +9195,8 @@
   A()
     : i(0), b(false) {
   }
+
+  ~A() {}
 };
 
 class B {
" >patch.txt
patch -p 1 < patch.txt
./configure --prefix=$PREFIX --disable-dependency-tracking --with-gmp=$PREFIX --disable-static
make -j$MAKEJ
make install
cd ..

tar xf cloog-ppl-0.15.11.tar.gz
cd cloog-ppl-0.15.11
./configure --prefix=$PREFIX --with-gmp=$PREFIX --with-ppl=$PREFIX --disable-static
make -j$MAKEJ
make install
cd ..

tar xf gcc-4.4.7.tar.bz2
cd gcc-4.4.7
mkdir build
cd build
if [ "$UNAME" == "Darwin" ]; then
  BUILD="--build=x86_64-apple-darwin15.2.0"
fi
export LD_RUN_PATH=$PREFIX/lib
export CPATH=$PREFIX/include
../configure $BUILD --prefix=$PREFIX --enable-languages=c,c++,fortran --with-gmp=$PREFIX --with-mpfr=$PREFIX --with-ppl=$PREFIX --disable-ppl-version-check --with-cloog=$PREFIX --with-system-zlib --enable-version-specific-runtime-libs --enable-libstdcxx-time=yes --enable-stage1-checking --enable-checking=release --disable-multilib --disable-werror
make -j$MAKEJ BOOT_CFLAGS="$BOOT_CFLAGS -D_FORTIFY_SOURCE=0" STAGE1_CFLAGS="$STAGE1_CFLAGS -std=gnu89 -D_FORTIFY_SOURCE=0 -fkeep-inline-functions"
make install
cd ../..

mkdir -p /opt/bib/devkit/bin
cd /opt/bib/devkit/bin
ln -s ../gcc44/bin/gcc .
ln -s ../gcc44/bin/g++ .
ln -s ../gcc44/bin/gfortran .
