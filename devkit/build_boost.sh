#!/bin/sh

set -e

MAKEJ=${MAKEJ-"2"}
UNAME=$(uname)
echo "MAKEJ=$MAKEJ"
echo "UNAME=$UNAME"

mkdir -p /opt/bib/devkit/src
cd /opt/bib/devkit/src

curl -LO http://downloads.sourceforge.net/project/boost/boost/1.55.0/boost_1_55_0.tar.bz2

tar xf boost_1_55_0.tar.bz2
cd boost_1_55_0
if [ "$UNAME" == "Darwin" ]; then
  echo "using darwin : : /opt/bib/devkit/bin/g++ ;" >user-config.jam
else
  echo "using gcc : : /opt/bib/devkit/bin/g++ ;" >user-config.jam
fi
./bootstrap.sh --prefix=/opt/bib/devkit/boost155 --libdir=/opt/bib/devkit/boost155/lib
./bjam --prefix=/opt/bib/devkit/boost155 --libdir=/opt/bib/devkit/boost155/lib -d2 -j$MAKEJ --layout=tagged --user-config=user-config.jam threading=multi link=shared install
cd ..
